关于PSI
-------------
>PSI是开源进销存系统。
>
>PSI是希腊字母Ψ的读音。
>
>产品寓意：PSI本身不完美，但追求的是不断改进的品质，距离终极产品就一步之遥(因为在希腊字母表中，Ψ之后是Ω，Ω有终极的意思)。

一张图看懂开源进销存PSI
-------------
<p>
    <img src="http://static.oschina.net/uploads/space/2015/0306/111629_RuyO_134395.jpg"/>
</p>

PSI演示
-------------
>PSI的演示见：<a href = 'http://psi.oschina.mopaas.com' target = '_blank'>http://psi.oschina.mopaas.com</a>

PSI 2015 下载地址
-------------
>PSI 2015下载地址：http://pan.baidu.com/s/1kTD2svX


源代码下载地址
-------------
>源码下载地址：http://git.oschina.net/crm8000/PSI/repository/archive?ref=master

通过PSI源码在本地安装
-------------
> 本地环境搭建: http://my.oschina.net/u/134395/blog/376530
> <br /> <br />
> 用WampServer部署可能会遇到的问题：http://my.oschina.net/u/134395/blog/383754
> <br /> <br />
> 用Nginx部署可能会遇到的问题：http://my.oschina.net/u/134395/blog/390650
> <br /> <br />
> 用IIS如何配置：http://my.oschina.net/u/1415918/blog/511228

PSI的开源协议
-------------
>PSI的开源协议为GPL v3

>如果您有Sencha Ext JS的商业许可（参见： http://www.sencha.com/legal/#Sencha_Ext_JS ），那么PSI的开源协议为Apache License v2。
>在Apache License协议下，您可以闭源并私有化PSI的代码，作为您自己的商业产品来销售。

更多文档
-------------
> 更多文档，请参考：http://my.oschina.net/u/134395


如需要技术支持，请联系
-------------
- QQ：1569352868
 <a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=1569352868&site=qq&menu=yes"><img border="0" src="http://wpa.qq.com/pa?p=2:1569352868:51" alt="点击这里给我发消息" title="点击这里给我发消息"/></a>

- Email：1569352868@qq.com

- QQ群：414474186
 <a target="_blank" href="http://shang.qq.com/wpa/qunwpa?idkey=64808ce24f2a3186ccb1f37aad9ed591bcc4fb257d09749753aca98c6c73e400"><img border="0" src="http://pub.idqqimg.com/wpa/images/group.png" alt="开源进销存PSI" title="开源进销存PSI"></a>

致谢
-------------
>PSI使用了如下开源软件，没有你们，就没有PSI
> 
>1、PHP (http://php.net/)
>
>2、MySQL (http://www.mysql.com/)
>
>3、ExtJS 4.2 (http://www.sencha.com/)
>
>4、ThinkPHP 3.2.3 (http://www.thinkphp.cn/)
>
>5、乱码 / pinyin_php (https://git.oschina.net/cik/pinyin_php)
>
>6、PHPExcel (http://phpexcel.codeplex.com/)
>
>7、TCPDF (http://www.tcpdf.org/)
>
>8、MUI (http://dev.dcloud.net.cn/mui/)